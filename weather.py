import requests
import sys
import datetime

args = sys.argv[1:]
klucz = args[0]
data = args[1] if len(args) > 1 else ''

url = "https://weatherapi-com.p.rapidapi.com/forecast.json"


def check_weather_data_for_given_day(given_date: str):
    if given_date == "":
        given_date = (datetime.date.today() + datetime.timedelta(days=1)).strftime("%Y-%m-%d")
    with open("dane_pogody.txt") as file:
        for line in file:
            if line.split("---")[0] == given_date:
                return line


def get_weather_condition():
    if data:
        querystring = {"q": "Niechorze", "dt": data}
    else:
        querystring = {"q": "Niechorze", "days": "2"}

    headers = {
        'x-rapidapi-host': "weatherapi-com.p.rapidapi.com",
        'x-rapidapi-key': klucz
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    print(response.status_code)

    if response.status_code != 200:
        return {"condition": "Brak danych", "date": "Brak danych"}

    dane = (response.json())

    if data:

        weather_condition = dane['current']['condition']['text']
        print(dane['forecast']['forecastday'][-1]['date'])
        return {"condition": weather_condition, "date": dane['forecast']['forecastday'][-1]['date']}

    else:
        weather_condition = dane['forecast']['forecastday'][-1]["day"]['condition']['text']
        print(dane['forecast']['forecastday'][-1]['date'])
        return {"condition": weather_condition, "date": dane['forecast']['forecastday'][-1]['date']}


def save_weather_to_file(data):
    with open("dane_pogody.txt", 'a') as file:
        if 'rain' in data['condition'].lower() or 'drizzle' in data['condition'].lower():
            file.write(f"{data['date']}---Będzie padać odpowiedź ze strony {data}\n")
        else:
            file.write(f"{data['date']}--- Nie będzie padać odpowiedź ze strony {data}\n")


def main():
    weather_from_file = check_weather_data_for_given_day(data)
    if weather_from_file:
        print("Pogoda dla wybranej daty została już sprawdzona.")
        print(weather_from_file)

    else:
        weather = get_weather_condition()
        save_weather_to_file(weather)


main()